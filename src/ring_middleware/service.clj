; Copyright 2013 Relevance, Inc.
; Copyright 2014-2022 Cognitect, Inc.

; The use and distribution terms for this software are covered by the
; Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0)
; which can be found in the file epl-v10.html at the root of this distribution.
;
; By using this software in any fashion, you are agreeing to be bound by
; the terms of this license.
;
; You must not remove this notice, or any other, from this software.

(ns ring-middleware.service
  (:require [clojure.java.io :as io]
            [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [io.pedestal.http.ring-middlewares :as middlewares]
            [ring.middleware.session.cookie :as cookie]))

(defn html-response
  [html]
  {:status 200 :body html :headers {"Content-Type" "text/html"}})

(defn intro-form
  "Prompt a user for their name, then remember it."
  [req]
  (html-response
   (slurp (io/resource "hello-form.html"))))

(defn introduction
  "Place the name provided by the user into their session, then send
   them to hello."
  [req]
  (let [name (get-in req [:params :name])
        name (if (empty? name) "Stranger" name)]
     (html-response (str "<html><body><h1>Hello, " name "!</h1></body></html>\n"))))

(def routes
  (let [session-interceptor (middlewares/session {:store (cookie/cookie-store)})]
    (route/expand-routes
     [[["/" {:get `intro-form}]
       ["/introduce" ^:interceptors [(body-params/body-params)
                                     middlewares/params
                                     middlewares/keyword-params
                                     session-interceptor]
        {:post `introduction}]]])))

(def service {:env                 :prod
              ::http/routes        routes
              ::http/resource-path "/public"
              ::http/type          :jetty
              ::http/port          8080})
